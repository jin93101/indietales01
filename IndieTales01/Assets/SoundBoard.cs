using UnityEngine;
using TMPro;

public class SoundBoard : MonoBehaviour
{
    //animace aktivity
    public bool soundIsReady = false;

    Animator anim;
    public Player player;
    public InGameManager igm;

    //procenta
    public TMP_Text soundTextPerc;

    //private float timer;
    private float stopTimer = 100f;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        if (igm.soundDone == stopTimer)
        {
            soundTextPerc.text = "Sound done: 100 %";
        }
        else
        {
            soundTextPerc.text = "Sound done: " + Mathf.Round(igm.soundDone) + " %";
        }

        if (soundIsReady == true)
        {
            anim.SetBool("isReady", true);
        }
        else
        {
            anim.SetBool("isReady", false);
        }

        if (player.isSound == true)
        {
            anim.SetBool("isSound", true);
        }
        else
        {
            anim.SetBool("isSound", false);
        }

        if (player.isSound == true)
        {
            igm.soundDone += Time.deltaTime;
            if (igm.soundDone >= stopTimer)
            {
                igm.soundDone = stopTimer;
                player.isSound = false;
            }
        }


    }
}
