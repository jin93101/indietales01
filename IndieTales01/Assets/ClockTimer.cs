using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class ClockTimer : MonoBehaviour
{

    public TMP_Text clockText;

    private float timer;
    private float baseTimer = 59f;//59f
    [SerializeField] private int countDown = 5;

    public InGameManager igm;

    void Start()
    {
        timer = baseTimer;
        countDown = 2;//4
    }

    
    void Update()
    {
        timer -= Time.deltaTime;
        if(timer <= 0)
        {
            timer = baseTimer;
            countDown--;
        }

        if(countDown == 5)
        {
            clockText.text = "05 : 00";
        }
        else if (countDown < 5 && countDown >= 0)
        {
            clockText.text = "0" + countDown + " : " + Mathf.Round(timer);
            if(timer <= 9 && timer >= 0)
            {
                clockText.text = "0" + countDown + " : 0" + Mathf.Round(timer);
            }
        }

        if(countDown < 0)
        {
            FinalScore();
            InGameManager.wasPublished = false;
            SceneManager.LoadScene("EndScene");
            //end the game and go to end screen
        }

        
    }

    public void FinalScore()
    {
        InGameManager.code = igm.codeDone;
        InGameManager.bug = igm.bugs;
        InGameManager.art = igm.artDone;
        InGameManager.sound = igm.soundDone;
        InGameManager.totalWords = igm.wordCount;
}
}
