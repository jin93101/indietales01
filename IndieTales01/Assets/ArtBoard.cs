using UnityEngine;
using TMPro;

public class ArtBoard : MonoBehaviour
{
    //animace aktivity
    public bool artIsReady = false;

    Animator anim;
    public Player player;
    public InGameManager igm;

    //procenta
    public TMP_Text artTextPerc;

    //private float timer;
    private float stopTimer = 100f;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        if (igm.artDone == stopTimer)
        {
            artTextPerc.text = "graphics done: 100 %";
        }
        else
        {
            artTextPerc.text = "graphics done: " + Mathf.Round(igm.artDone) + " %";
        }

        if (artIsReady == true)
        {
            anim.SetBool("isReady", true);
        }
        else
        {
            anim.SetBool("isReady", false);
        }

        if (player.isArt == true)
        {
            anim.SetBool("isArt", true);
        }
        else
        {
            anim.SetBool("isArt", false);
        }

        if (player.isArt == true)
        {
            igm.artDone += Time.deltaTime;
            if (igm.artDone >= stopTimer)
            {
                igm.artDone = stopTimer;
                player.isArt = false;
            }
        }


    }
}
