using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class EndScreenManager : MonoBehaviour
{
    //TEXT
    public TMP_Text code;
    public TMP_Text art;
    public TMP_Text sound;
    //public TMP_Text exhausted;
    public TMP_Text bugs;
    public TMP_Text words;

    public GameObject winScenario01;
    public GameObject winScenario02;
    public GameObject winScenario03;
    public GameObject looseScenario;

    private void Start()
    {
        if(InGameManager.wasPublished == true)
        {
            if(InGameManager.bug >= 25)
            {
                winScenario02.SetActive(true);
            }else if(InGameManager.totalWords >= 50)
            {
                winScenario03.SetActive(true);
            }else
            {
                winScenario01.SetActive(true);
            }
            
        }else
        {
            looseScenario.SetActive(true);
        }


        code.text = "% of code done: " + InGameManager.code.ToString() + " %";
        art.text = "% of art done: " + InGameManager.art.ToString() + "%";
        sound.text = "% of sounds done: " + InGameManager.sound.ToString() + " %";
        bugs.text = "You did " + InGameManager.bug.ToString() + " bugs in your code"; 
        words.text = "You did write " + InGameManager.totalWords.ToString() + " words into your code";
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            SceneManager.LoadScene("SampleScene");
        }
    }
}
