using UnityEngine.SceneManagement;
using UnityEngine;
using TMPro;

public class EntryScene : MonoBehaviour
{

    public int buttonInteraction = 0;

    public TMP_Text entryTalk01Text;
    public TMP_Text entryTalk02Text;
    public TMP_Text entryTalk03Text;


    public GameObject entryTalk01;
    public GameObject entryTalk02;
    public GameObject entryTalk03;

    private void Start()
    {
        buttonInteraction = 0;
    }

    public void ContinueButtonPressed()
    {
        buttonInteraction++;
    }

    
    void Update()
    {
        switch (buttonInteraction)
        {
            case 1:
                entryTalk01.SetActive(false);
                entryTalk02.SetActive(true);
                break;
            case 2:
                entryTalk02.SetActive(false);
                entryTalk03.SetActive(true);
                break;
            case 3:
                SceneManager.LoadScene("SampleScene");
                break;
        }    
    }
}
