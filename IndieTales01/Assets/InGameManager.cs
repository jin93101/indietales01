using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InGameManager : MonoBehaviour
{

    public int bugs = 0;
    public float codeDone = 0f;
    public float artDone;
    public float soundDone;
    public float energy;
    private float baseEnegry = 100f;
    public float timeClock;

    public int wordCount;

    //FINAL SCORES
    public static float code;
    public static float art;
    public static int bug;
    public static float sound;
    public static bool wasPublished;
    public static int totalWords;

    //TEXT AREA
    public TMP_Text energyText;
    public TMP_Text bugsText;
    public TMP_Text timeClockText;
    public TMP_Text codeDoneText;

    //ENERGY
    private float baseEnergyDrain = 0.002f;
    private float codingEnergyDrain = 0.02f;
    private float sleepEnergyGain = 0.05f;
    private float exhaustEnergyGain = 0.03f;

    public Player player;
    public GameObject cmg;

    private void Start()
    {
        energy = baseEnegry;
    }

    private void Update()
    {

        energyText.text = "energy " + Mathf.Round(energy) + " / 100";
        bugsText.text = "Bugs: " + bugs;
        codeDoneText.text = "% of code done: " + Mathf.Round(codeDone) + " %";

        if(energy <= 50f && energy > 15f)
        {
            energyText.color = Color.yellow;
        }else if(energy <= 15f)
        {
            //could try to make some blinking effect here
            energyText.color = Color.red;
        }else
        {
            energyText.color = Color.green;
        }

        if (player.isCoding == true || player.isArt == true || player.isSound == true)
        {
            energy -= codingEnergyDrain;
        }else if (player.isCoding != true && player.isSleeping != true && player.isExhausted != true && player.isArt != true && player.isSound != true)
        {
            energy -= baseEnergyDrain;
        }else if(player.isSleeping == true && player.startSleep == true)
        {
            energy += sleepEnergyGain;
        }

        if(energy >= 100f)
        {
            energy = 100f;
        }

        if(energy <= 0)
        {
            energy = 0;
        }

        //EXHAUST
        if(player.isExhausted == true)
        {
            energy += exhaustEnergyGain;
        }
        
        if(player.isCoding == true)
        {
            cmg.SetActive(true);
        }else if (player.isCoding == false)
        {
            cmg.SetActive(false);
        }
    }
}
