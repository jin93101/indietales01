using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WordGenerator : MonoBehaviour
{

    private static string[] wordList = {"aseprite", "bool", "commit", "development", "euler", "foreach", "getComponent", 
                                        "hope", "indieTales", "joker", "kit", "loadScene", "mock", "null", 
                                        "overLoad", "pull", "return;", "quaternion", "string", "time.DeltaTime", "Unity", "var", "wait", "x-axis", "y-axis", "zero",
    "Adam", "Bedtime", "CTRL", "DOOM", "Error", "Faster", "Gravity", "Heavy", "Isometric", "Jin93101", "K.O.", "Long", "McHammer", "Noob", "One", "Package.JSON", 
        "RocketScience", "Quit", "SoundOfSilence", "Tetris", "u-Turn", "Vendetta", "Walrus", "Xena", "Yesterday", "Zen" };

    public static string GetRandomWord()
    {
        int randomIndex = Random.Range(0, wordList.Length);
        string randomWord = wordList[randomIndex];

        return randomWord;
    }
}
