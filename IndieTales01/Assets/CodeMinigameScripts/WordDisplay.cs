using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WordDisplay : MonoBehaviour
{
    InGameManager igm;
    private float codeDoneIncrement = .25f;
    private float codeDoneHighInc = .75f;
    private float codeDoneTopInc = 1f;
    private float codeDoneLowInc = .05f;
    private float bugPenalty = 2f;

    public Text text;
    private float fallSpeed = 40f;

    public float myY;
    private float verticalMid = Screen.height / 2;
    private float startY;

    private void Start()
    {
        igm = FindObjectOfType<InGameManager>();
        startY = verticalMid + 150f;
    }

    public void SetWord(string word)
    {
        text.text = word;
    }

    public void RemoveLetter()
    {
        text.text = text.text.Remove(0, 1);
        text.color = Color.green;
    }

    public void RemoveWord()
    {
        if(igm.energy > 90f)
        {
            igm.codeDone += codeDoneTopInc;
        }
        else if(igm.energy <= 90 && igm.energy > 75)
        {
            igm.codeDone += codeDoneHighInc;
        }
        else if(igm.energy <= 75 && igm.energy < 35f)
        {
            igm.codeDone += codeDoneIncrement;
        }
        else if (igm.energy <= 35f)
        {
            igm.codeDone += codeDoneLowInc;
        }
        igm.wordCount++;
        Destroy(gameObject);
    }

    private void Update()
    {
        transform.Translate(0f, -fallSpeed * Time.deltaTime, 0f);
        myY = transform.position.y;
        
        if (myY <= verticalMid - 100f)
        {
            //get bug count
            igm.bugs++;
            //bugs decreases the energy
            igm.energy -= bugPenalty;

            Vector3 pos = new Vector3(transform.position.x, startY, transform.position.z);
            transform.position = pos;
        }
        
    }
}
