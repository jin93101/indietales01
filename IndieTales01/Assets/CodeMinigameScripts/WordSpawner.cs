using UnityEngine;

public class WordSpawner : MonoBehaviour
{

    public GameObject wordPrefab;
    public Transform wordCanwas;

    private float verticalMid = Screen.height/2;
    private float horizontalMid = Screen.width/2;


    public WordDisplay SpawnWord()
    {

        Vector3 randomPosition = new Vector3(Random.Range(horizontalMid - 100f, horizontalMid + 150f), verticalMid + 150f);

        GameObject wordObj = Instantiate(wordPrefab, randomPosition, Quaternion.identity, wordCanwas);
        WordDisplay wordDisplay = wordObj.GetComponent<WordDisplay>();

        return wordDisplay;
    }
}
