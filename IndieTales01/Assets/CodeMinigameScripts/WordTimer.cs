using UnityEngine;

public class WordTimer : MonoBehaviour
{
    public WordManager wordManager;

    [SerializeField] private float wordDelay = 2f;
    private float baseOfWordDelay = 2f;
    [SerializeField] private float nextWordTime = 0f;

    private float resetTimer = 20f;
    [SerializeField] private float thisTimer;


    public InGameManager igm;

    private void Start()
    {
        thisTimer = resetTimer;
    }

    private void Update()
    {
        if (thisTimer <= 0)
        {
            wordDelay = baseOfWordDelay;
            thisTimer = resetTimer;
        }else
        {
            thisTimer -= Time.deltaTime;
        }


            if (Time.time >= nextWordTime)
            {
                wordManager.AddWord();
                nextWordTime = Time.time + wordDelay;
                wordDelay *= .99f;
            }

    }
}
