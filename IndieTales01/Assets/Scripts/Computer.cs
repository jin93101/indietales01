using UnityEngine;

public class Computer : MonoBehaviour
{
    public bool pcReady = false;

    Animator anim;
    public Player player;


    void Start()
    {
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        if (pcReady == true)
        {
            anim.SetBool("ready", true);
        }
        else
        {
            anim.SetBool("ready", false);
        }

        if (player.isCoding == true)
        {
            anim.SetBool("isActive",true);
            anim.SetBool("ready",false);
        } else if (player.isCoding == false)
        {
            anim.SetBool("isActive",false);
        }
    }
}
