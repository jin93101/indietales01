using UnityEngine;

public class Bed : MonoBehaviour
{
    public bool bedIsReady = false;

    Animator anim;
    public Player player;

    
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if(player.isSleeping == true)
        {
            anim.SetBool("sleep", true);
        }else
        {
            anim.SetBool("sleep", false);
            anim.SetBool("isIdle", true);
        }

        if(bedIsReady == true)
        {
            anim.SetBool("ready", true);
        }else
        {
            anim.SetBool("ready", false);
            anim.SetBool("isIdle", true);
        }
    }
}
