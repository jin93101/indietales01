using UnityEngine;

public class Chair : MonoBehaviour
{
    Animator anim;
    public Player player;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        if(player.isCoding == true)
        {
            anim.SetBool("isCoding", true);
        }else
        {
            anim.SetBool("isCoding", false);
        }
    }
}
