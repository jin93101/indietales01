using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    private float walkSpeed = 5f;
    private float horizontalMove;
    private bool faceRight = true;


    //states
    public bool freeMovement = true;
    public bool isCoding = false;
    public bool isSleeping = false;
    public bool isEating = false;

    public bool isSound = false;
    public GameObject soundPerc;
    public GameObject cancelSound;

    public bool isArt = false;
    public GameObject artPerc;
    public GameObject cancelArt;

    public bool isPublishing = false;
    public bool publishingDone = false;
    public GameObject publishPerc;
    public GameObject cancelPublish;
    public GameObject publishDoneText;

    //EXHAUSTED means that player was under 5% of energy. While exhausted, player can' t do anything, just wait till he recovers till 25% of energy
    public bool isExhausted = false;
    public bool afterExhaust = false;

    [SerializeField] private bool byComputer = false;
    [SerializeField] private bool byBed = false;
    [SerializeField] private bool byPublishBoard = false;
    [SerializeField] private bool byArtBoard = false;
    [SerializeField] private bool bySoundBoard = false;

    //TEXT AREA
    public TMP_Text pressToCode;
    public TMP_Text pressToSleep;
    public TMP_Text pressToWakeUp;
    public TMP_Text pressToPublish;
    public TMP_Text pressToArt;
    public TMP_Text pressToSound;
    //public TMP_Text ;
    public GameObject pressToCodeText;
    public GameObject pressToSleepText;
    public GameObject pressToWakeUpText;
    public GameObject pressToPublishText;
    public GameObject pressToArtText;
    public GameObject pressToSoundText;
    public GameObject exhaustedText;

    //SLEEPING
    private float sleepTimer;
    private float sleepTimerBase = 2.5f;
    public bool startSleep = false; //sending this info to InGameManager
    //game needs to wait approx 5s before the sleep regeneration starts


    Rigidbody2D rb;
    Animator anim;
    SpriteRenderer sr;
    BoxCollider2D bc;

    public InGameManager igm;
    public Bed bed;
    public Computer monitor;
    public PublishBoard publishBoard;
    public SoundBoard soundBoard;
    public ArtBoard artBoard;
    public ClockTimer clockTimer;

    private void Start()
    {
        sleepTimer = sleepTimerBase;
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        sr = GetComponent<SpriteRenderer>();
        bc = GetComponent<BoxCollider2D>();
    }

    private void Update()
    {
        pressToCode.text = "press [e] to code";
        pressToSleep.text = "press [e] to sleep";
        pressToWakeUp.text = "press [space] to wake up";

        //EXHAUSTED
        if(igm.energy <= 5f)
        {
            isExhausted = true;
            sr.enabled = true;
            
            //exhaust fall on the floor animation
        }
        else if(igm.energy >= 25f && isExhausted == true)
        {
            freeMovement = true;
            anim.SetBool("isExhausted", false);
            //wakeup from the floor animation
            isExhausted = false;
        }

        if(isExhausted == true)
        {
            bc.enabled = false;
            exhaustedText.SetActive(true);
            freeMovement = false;
            isCoding = false;
            anim.SetBool("isExhausted", true);
            anim.SetBool("isWalking", false);
            //idle exhaust animation
        }
        if(isExhausted == false)
        {
            bc.enabled = true;
            exhaustedText.SetActive(false);
        }
        //END OF EXHAUSTION

        horizontalMove = Input.GetAxis("Horizontal");

        if(horizontalMove == 0)
        {
            //set idle animation
            anim.SetBool("isWalking", false);
        }else
        {
            //play run animation
            anim.SetBool("isWalking", true);
        }

        //CODING PART
        if(byComputer == true && Input.GetKeyDown(KeyCode.E))
        {
            pressToCodeText.SetActive(false);
            freeMovement = false;
            isCoding = true;
            sr.enabled = false;
        }
        if (byComputer == true && isCoding != true)
        {
            monitor.pcReady = true;
        }
        else if (byComputer == false || isCoding == true)
        {
            monitor.pcReady = false;
        }

        //SLEEPING & BED PART
        if (byBed == true && Input.GetKeyDown(KeyCode.E))
        {
            pressToSleepText.SetActive(false);
            freeMovement = false;
            isSleeping = true;
            sr.enabled = false;
        }
        if(byBed == true)
        {
            bed.bedIsReady = true;
        } else if (byBed == false)
        {
            bed.bedIsReady = false;
        }

        //PUBLISH PART
        if (byPublishBoard == true && Input.GetKeyDown(KeyCode.E))
        {
            cancelPublish.SetActive(true);
            publishPerc.SetActive(true);
            pressToPublishText.SetActive(false);
            freeMovement = false;
            isPublishing = true;
            sr.enabled = false;
        }
        if (byPublishBoard == true)
        {
            publishBoard.publishIsReady = true;
        }else if (byPublishBoard == false)
        {
            publishBoard.publishIsReady = false;
        }

        if(publishingDone == true)
        {
            cancelPublish.SetActive(false);
            publishDoneText.SetActive(true);
        }

        if(publishingDone == true && Input.GetKeyDown(KeyCode.Y))
        {
            InGameManager.wasPublished = true;
            clockTimer.FinalScore();
            SceneManager.LoadScene("EndScene");
        }
        else if(publishingDone == true && Input.GetKeyDown(KeyCode.N))
        {
            publishDoneText.SetActive(false);
            publishingDone = false;
            sr.enabled = true;
            freeMovement = true;
        }

        //SOUND PART
        if (bySoundBoard == true && Input.GetKeyDown(KeyCode.E))
        {
            cancelSound.SetActive(true);
            soundPerc.SetActive(true);
            pressToSoundText.SetActive(false);
            freeMovement = false;
            isSound = true;
            sr.enabled = false;
        }
        if (bySoundBoard == true)
        {
            soundBoard.soundIsReady = true;
        }
        else if (bySoundBoard == false)
        {
            soundBoard.soundIsReady = false;
        }

        //ART PART
        if (byArtBoard == true && Input.GetKeyDown(KeyCode.E))
        {
            cancelArt.SetActive(true);
            artPerc.SetActive(true);
            pressToArtText.SetActive(false);
            freeMovement = false;
            isArt = true;
            sr.enabled = false;
        }
        if (byArtBoard == true)
        {
            artBoard.artIsReady = true;
        }
        else if (byArtBoard == false)
        {
            artBoard.artIsReady = false;
        }


        //CANCEL ANY ACTIVE ACTIVITY
        if (freeMovement == false && Input.GetKeyDown(KeyCode.Space) && publishingDone != true)
        {
            freeMovement = true;
            if (isCoding == true)
            {
                isCoding = false;
                sr.enabled = true;
            }
            if (isPublishing == true)
            {
                isPublishing = false;
                sr.enabled = true;
                cancelPublish.SetActive(false);
                publishPerc.SetActive(false);
            }
            if(isSound == true)
            {
                isSound = false;
                sr.enabled = true;
                cancelSound.SetActive(false);
                soundPerc.SetActive(false);
            }
            if (isArt == true)
            {
                isArt = false;
                sr.enabled = true;
                cancelArt.SetActive(false);
                artPerc.SetActive(false);
            }
            return;
        }

        //SLEEPING TIMER
        if(isSleeping == true)
        {
            sleepTimer -= Time.deltaTime;
            if(sleepTimer <= 0)
            {
                startSleep = true;
            }
        }

        if(isSleeping == true && startSleep == true)
        {
            pressToWakeUpText.SetActive(true);
        }

        if (startSleep == true && Input.GetKeyDown(KeyCode.Space))
        {
                isSleeping = false;
                sr.enabled = true;
                pressToWakeUpText.SetActive(false);
                startSleep = false;
                sleepTimer = sleepTimerBase;
        }
    }

    private void FixedUpdate()
    {
        if(freeMovement != false)
        {
            Move();
            if (faceRight == false && horizontalMove > 0)
            {
                Flip();
            }
            else if (faceRight == true && horizontalMove < 0)
            {
                Flip();
            }
        }
        
       
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Bed"))
        {
            //DISPLAY MESSAGE
            pressToSleepText.SetActive(true);
            byBed = true;
        }

        if (collision.CompareTag("Computer"))
        {
            //DISPLAY MESSAGE
            pressToCodeText.SetActive(true);
            byComputer = true;
        }

        if (collision.CompareTag("publishBoard"))
        {
            //DISPLAY MESSAGE
            pressToPublishText.SetActive(true);
            byPublishBoard = true;
        }

        if (collision.CompareTag("artBoard"))
        {
            //DISPLAY MESSAGE
            pressToArtText.SetActive(true);
            byArtBoard = true;
        }

        if (collision.CompareTag("soundBoard"))
        {
            //DISPLAY MESSAGE
            pressToSoundText.SetActive(true);
            bySoundBoard = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        byBed = false;
        byComputer = false;
        bySoundBoard = false;
        byArtBoard = false;
        byPublishBoard = false;

        pressToSleepText.SetActive(false);
        pressToCodeText.SetActive(false);
        pressToPublishText.SetActive(false);
        pressToArtText.SetActive(false);
        pressToSoundText.SetActive(false);
}

    private void Move()
    {
        float x = horizontalMove;
        float moveBy = x * walkSpeed;
        rb.velocity = new Vector2(moveBy, rb.velocity.y);
    }

    private void Flip()
    {
        faceRight = !faceRight;
        transform.Rotate(0f, 180f, 0f);
    }
}
