using UnityEngine;
using TMPro;

public class PublishBoard : MonoBehaviour
{
    public bool publishIsReady = false;

    Animator anim;
    public Player player;

    public TMP_Text publishTextPerc;

    private float timer;
    private float stopTimer = 10f;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        if(timer == stopTimer)
        {
            publishTextPerc.text = "publishing: 100 %";
        }else
        {
            publishTextPerc.text = "publishing: " + Mathf.Round(timer * 10) + " %";
        }
        
        if (publishIsReady == true)
        {
            anim.SetBool("isReady", true);
        }
        else
        {
            anim.SetBool("isReady", false);
        }

        if(player.isPublishing == true)
        {
            anim.SetBool("isPublishing", true);
        }else
        {
            anim.SetBool("isPublishing", false);
        }

        if(player.isPublishing == true)
        {
            timer += Time.deltaTime;
            if(timer >= stopTimer)
            {
                timer = stopTimer;
                player.publishingDone = true;
                player.isPublishing = false;
            }
        }


    }
}
